
//Activity 1

let number = prompt("Enter a number here:");


if (number <= 50){
    console.log("The value must be greater than 50!");

} else {

    for (number; number >= 0; number--){
        let moduloTen = number % 10;
        let moduloFive = number % 5;
        if (moduloTen === 0){
            console.log("The number is divisible by 10. Skipping the number.");
            continue
        } else if (number <= 51){
            console.log("Current value is 50. Loop termnated.");
            break
        } else if (moduloFive === 0){
            console.log(number);
        } else {
            continue
        }

    }
}

// Activity 2

let longWord = "supercalifragilisticexpialidocious";
let consonantsOnly = "";

console.log(longWord);

for (let i = 0; i <longWord.length; i++){

    iterLetters = longWord[i].toLowerCase();

    if(iterLetters === 'a' || iterLetters === 'e' || iterLetters === 'i' || iterLetters === 'o' || iterLetters === 'u'){
        continue;

    } else {
        
        consonantsOnly += longWord[i];

        console.log("end of loop");
    }

}

console.log(consonantsOnly);